# -*- coding: utf-8 -*-
"""
Author: Christian Cabrera
cabrerac@scss.tcd.ie

"""

import json
from sklearn.cluster import AgglomerativeClustering
from scipy.cluster.hierarchy import dendrogram
import numpy as np
import pandas as pd

# This function computes the distance between two trips
def computeDistance(links1, links2):
    allLinks = list(set(links1) | set(links2))
    sharedLinks = list(set(links1) & set(links2))   
    distance = 1 - (len(sharedLinks)/len(allLinks))
    return distance

# This function computes the distance matrix for the trips if the matrix does not exist or (t==0)
# read it from a file (t==1)
def getMatrix(trips, t):
    matrix = []
    if t == 0:
        matrix = np.zeros((len(trips), len(trips)))
        i = 0
        while i < len(trips):
            trip1 = trips[str(i)]
            links1 = trip1["links"]
            j = 0
            while j < len(trips):
                trip2 = trips[str(j)]
                links2 = trip2["links"]
                distance = computeDistance(links1, links2)
                matrix[i][j] = distance
                j = j + 1
            i = i + 1            
        np.savetxt('./matrix.csv', matrix, delimiter=',')
    if t == 1:
        matrix = np.loadtxt(open('./matrix.csv', 'rb'), delimiter=',')        
    return matrix

# This function plots a dendogram that shows the clustering process result
def plot_dendrogram(model, **kwargs):
    # Create linkage matrix and then plot the dendrogram

    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count
    linkage_matrix = np.column_stack([model.children_, model.distances_,
                                      counts]).astype(float)

    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, **kwargs)

# This function groups trips by cluster
def getClusterTrips(trips, predicted):
    clustersTrips = {}
    t = 0
    while t < len(trips):
        trip = trips[str(t)]
        tripCluster = str(predicted[t])
        if tripCluster not in clustersTrips:
            clusterTrips = []
            clusterTrips.append(trip)
            clustersTrips[tripCluster] = clusterTrips
        else:
            clusterTrips = clustersTrips[tripCluster]
            clusterTrips.append(trip)
            clustersTrips[tripCluster] = clusterTrips
        t = t + 1
    return clustersTrips

# This calculates the frequency matrix for the Hidden Markov Model
def getFrequencyMatrix(trips, n_clusters, predicted):
    frequencyMatrix = []
    links = []
    clusters = []
    for key, trip in trips.items():
        links = list(set(links) | set(trip['links']))
    
    frequencyMatrix = np.zeros((len(links), n_clusters))
    t = 0
    while t < len(trips):
        trip = trips[str(t)]
        tripCluster = predicted[t]
        if tripCluster not in clusters:
            clusters.append(tripCluster)
        tripLinks = trip['links']
        l = 0
        while l < len(links):
            count = tripLinks.count(links[l])
            if count > 0:
                frequencyMatrix[l,tripCluster] = frequencyMatrix[l,tripCluster] + count
            l = l + 1
        t = t + 1
                    
    clusters.sort()
    df = pd.DataFrame(frequencyMatrix, columns=clusters, index=links)
    return df

# This calculates a transition matrices for the Bayesian Classifier
def getTransitionMatrix(trips, n_clusters, predicted):
    transitionMatrices = {}
    linksIndexes = {}
    links = []
    for key, trip in trips.items():
        links = list(set(links) | set(trip['links']))
        
    i = 0
    for link in links:
        linksIndexes[link] = i
        i = i + 1
    
    c = 0    
    while c < n_clusters:
        transitionMatrix = []
        transitionMatrix = np.zeros((len(links), len(links)))
        transitionMatrices[c] = transitionMatrix
        c = c + 1
        
    transitions = []
    t = 0
    while t < len(trips):
        trip = trips[str(t)]
        cluster = predicted[t]
        tripLinks = trip['links']    
        transitions = getTransitions(tripLinks, transitions, cluster)
        t = t + 1
    
    transitionedFrom = getTransitionedFrom(transitions)
    transitionsFromTo = getTransitionsFromTo(transitions)
    
    for cluster, transitionFromTo in transitionsFromTo.items():
        transitionMatrix = transitionMatrices[cluster]
        tf = transitionedFrom[cluster]
        for t, froms in transitionFromTo.items():
            for f, value in froms.items():
                if f in tf.keys():
                    transitioned = tf[f]
                    a = value/transitioned
                    i = linksIndexes[f] 
                    j = linksIndexes[t]
                    transitionMatrix[i][j] = a
        transitionMatrices[cluster] = transitionMatrix
        
    for cluster, transitionMatrix in transitionMatrices.items():
        df = pd.DataFrame(transitionMatrix, columns=links, index=links)
        transitionMatrices[cluster] = df
    return transitionMatrices

# This function returns the transitions between a trip links
def getTransitions(tripLinks, trans, cluster):
    transitions = trans
    index = 0
    while index < (len(tripLinks) - 1):
        link1 = tripLinks[index]
        link2 = tripLinks[index+1]
        transition = {}
        transition["from"] = link1
        transition["to"] = link2
        transition["cluster"] = cluster
        transitions.append(transition)
        index = index + 1
    return transitions

# This function returns the number of times that links were transitioned from
def getTransitionedFrom(transitions):
    transitionedFrom = {}    
    for transition in transitions:
        cluster = transition['cluster']
        f = transition['from']
        if cluster in transitionedFrom.keys():
            tfc = transitionedFrom[cluster]
            if f in tfc.keys():
                tf = tfc[f] + 1
                tfc[f] = tf
            else:
                tfc[f] = 1
            transitionedFrom[cluster] = tfc
        else:
            tfc = {}
            tfc[f] = 1
            transitionedFrom[cluster] = tfc    
    return transitionedFrom

# This function returns the number of times that links were transitioned to
def getTransitionsFromTo(transitions):
    transitionsFromTo = {}
    for transition in transitions:
        cluster = transition['cluster']
        t = transition['to']
        if cluster in transitionsFromTo.keys():
            tftc = transitionsFromTo[cluster]
            if t in tftc.keys():
                tft = tftc[t]
                f = transition['from']
                if f in tft:
                    tf = tft[f] + 1
                    tft[f] = tf
                    tftc[t] = tft
                    transitionsFromTo[cluster] = tftc
                else:
                    tft[f] = 1
                    tftc[t] = tft
                    transitionsFromTo[cluster] = tftc
            else:
                tft = {}
                tft[f] = 1
                tftc[t] = tft
                transitionsFromTo[cluster] = tftc
        else:                  
            tft = {}
            tf = {}
            f = transition['from']
            tf[f] = 1
            tft[t] = tf
            transitionsFromTo[cluster] = tft
        
    return transitionsFromTo

# This function calculates the initial probabilities for the Bayesian Classifier
def getInitialProbabilities(trips, n_clusters, predicted):
    initialProbabilities = []
    clusters = []
    
    linksIndexes = {}
    links = []
    for key, trip in trips.items():
        links = list(set(links) | set(trip['links']))
        
    i = 0
    for link in links:
        linksIndexes[link] = i
        i = i + 1
    
    initialProbabilities = np.zeros((len(links), n_clusters))
    
    tripsPerCluster = {}
    t = 0
    while t < len(trips):
        trip = trips[str(t)]
        tripLinks = trip['links']
        link = tripLinks[0]
        cluster = predicted[t]
        if cluster not in clusters:
            clusters.append(cluster)
        if cluster not in tripsPerCluster:
            tpc = {}
            tpc['trips'] = 1
            clusterLinks = {}
            clusterLinks[link] = 1
            tpc['clusterLinks'] = clusterLinks
            tripsPerCluster[cluster] = tpc
            
        else:
            tpc = tripsPerCluster[cluster]
            nt = tpc['trips']
            nt = nt + 1
            tpc['trips'] = nt
            clusterLinks = tpc['clusterLinks']            
            
            if link not in clusterLinks:
                clusterLinks[link] = 1
            else:
                nl = clusterLinks[link]
                nl = nl + 1
                clusterLinks[link] = nl
            tpc['clusterLinks'] = clusterLinks
            tripsPerCluster[cluster] = tpc
        t = t + 1
        
    for cluster, tpc in tripsPerCluster.items():
        ntc = tpc['trips']
        clusterLinks = tpc['clusterLinks']
        
        for clusterLink, value in clusterLinks.items():
            p = value/ntc
            linkIndex = linksIndexes[clusterLink]
            initialProbabilities[linkIndex][cluster] = p
            
    df = pd.DataFrame(initialProbabilities, columns=clusters, index=links)
    return df

# This function calculates the cluster probabilities for the Bayesian Classifier
def getClusterProbabilities(trips, n_clusters, predicted):
    clusterProbabilities = []
    clusterProbabilities = np.zeros((n_clusters))
    clusters = [] 
    tripsPerCluster = {}
    t = 0
    while t < len(trips):
        cluster = predicted[t]
        if cluster not in tripsPerCluster:
            tripsPerCluster[cluster] = 1
            clusters.append(cluster)
        else:
            tpc = tripsPerCluster[cluster]
            tpc = tpc + 1
            tripsPerCluster[cluster] = tpc
        t = t + 1
        
    for cluster, tpc in tripsPerCluster.items():
        p = tpc/len(trips)
        clusterProbabilities[cluster] = p
        
    df = pd.DataFrame(clusterProbabilities, index=clusters, columns=['PC'])
    return df

def main():
    # Historical trips reading
    file = open('./trips.json')
    trips = json.load(file)
    
    # Trips clustering
    matrix = getMatrix(trips, 1)
    model = AgglomerativeClustering(n_clusters=None, affinity='precomputed', linkage='average', compute_full_tree=True, distance_threshold=0.2)
    clusters = model.fit(matrix)
    plot_dendrogram(clusters, truncate_mode='level')
    predicted = model.fit_predict(matrix)
    clusterTrips = getClusterTrips(trips, predicted)
    with open('clusterTrips.json', 'w') as fp:
        json.dump(clusterTrips, fp)    
    
    # Frequency matrix for Hidden Markov Model
    frequencyMatrix = getFrequencyMatrix(trips, clusters.n_clusters_, predicted)
    frequencyMatrix.to_csv('./frequencyMatrix.csv')
    
    # Transition matrices for Bayesian Classifier
    transitionMatrices = getTransitionMatrix(trips, clusters.n_clusters_, predicted)
    for cluster, transitionMatrix in transitionMatrices.items():
        transitionMatrix.to_csv('./transitionMatrix_'+str(cluster)+'.csv')
    # Initial probabilities for Bayesian Classifier
    initialProbabilities = getInitialProbabilities(trips, clusters.n_clusters_, predicted)
    initialProbabilities.to_csv('./initialProbabilities.csv')
    # Cluster probabilities for Bayesian Classifier
    clusterProbabilities = getClusterProbabilities(trips, clusters.n_clusters_, predicted)
    clusterProbabilities.to_csv('./clusterProbabilities.csv')
        
main()