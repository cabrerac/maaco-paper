# -*- coding: utf-8 -*-
"""
Created on Tue Sep  8 19:14:37 2020

@author: Administrator
"""

import seaborn as sns
import csv
import pandas as pd
import matplotlib.pyplot as plt
csfont = {'fontname':'Times New Roman'}

# This function plots the bar plots that include all approaches results 
def plot_all_rounds():
    SMALL_SIZE = 20
    MEDIUM_SIZE = 32
    LARGE_SIZE = 32
    plt.rc('font', size=SMALL_SIZE, family='Times New Roman')          
    plt.rc('axes', titlesize=MEDIUM_SIZE)
    plt.rc('axes', labelsize=MEDIUM_SIZE)    
    plt.rc('axes', linewidth=2.0)
    plt.rc('xtick', labelsize=28)    
    plt.rc('ytick', labelsize=SMALL_SIZE)    
    plt.rc('legend', fontsize=SMALL_SIZE)
    plt.rc('figure', titlesize=LARGE_SIZE)
    
    data = []
    headers = []
    #baselines = ['bestfit', 'closestfit', 'ilp']
    baselines = ['sa', 'bestfit', 'closestfit', 'ilp', 'maxfit', 'multiopt']
    approaches = []
    approaches = ['maaco-BC', 'maaco-HMM', 'aco']
    scenarios = [1, 2]
    #scenarios = [1, 2, 3]
    #servers = [100, 300]
    servers = [100, 200, 300] 
    #functions = [1]
    functions = [1, 3, 5]
    iterations = [10]
    #iterations = [100, 1000, 10000]
    ants = [1000]
    #ants = [10, 100, 1000]
    rounds = [1, 2, 3, 4, 5]
    
    for scenario in scenarios:
        iterations = [10]
        ants = [1000]
        fig1, axs1 = plt.subplots(3, 3, figsize=(30, 30), sharex = False)
        fig2, axs2 = plt.subplots(3, 3, figsize=(30, 30), sharex = False)
        fig3, axs3 = plt.subplots(3, 3, figsize=(30, 30), sharex = False)
        """if scenario == 1:
            fig1.suptitle('Average Network Latency (ms) Scenario 1: $\u03bc_s > \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig2.suptitle('Resource Utilisation (\u03C3) Scenario 1: $\u03bc_s > \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig3.suptitle('Average Waiting Time (s) Scenario 1: $\u03bc_s > \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
        if scenario == 2:
            fig1.suptitle('Average Network Latency (ms) Scenario 2: $\u03bc_s < \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig2.suptitle('Resource Utilisation (\u03C3) Scenario 2: $\u03bc_s < \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig3.suptitle('Average Waiting Time (s) Scenario 2: $\u03bc_s < \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
        if scenario == 3:
            fig1.suptitle('Average Network Latency (ms) Scenario 3: $\u03bc_s = \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig2.suptitle('Resource Utilisation (\u03C3) Scenario 3: $\u03bc_s = \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig3.suptitle('Average Waiting Time (s) Scenario 3: $\u03bc_s = \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)"""
        i = 0
        for function in functions:
            j = 0
            for server in servers:
                for approach in approaches:
                    for iteration in iterations:
                        for ant in ants:
                            if approach == 'maaco-HMM':
                                file = './data/maaco/decentralised/'
                                file = file + 'approach_maaco'
                            elif approach == 'maaco-BC':
                                file = './data/maaco/decentralised/'
                                file = file + 'approach_maaco'
                            else:
                                file = './data/'+approach+'/decentralised/'
                                file = file + 'approach_' + approach
                                                           
                            file = file + '_scenario_' + str(scenario)
                            file = file + '_edge_servers_' + str(server)
                            file = file + '_mec_systems_5'
                            file = file + '_functions_' + str(function)
                            if approach == 'maaco-HMM':
                                file = file + '_prediction_HMM'
                            elif approach == 'maaco-BC':
                                file = file + '_prediction_BC'
                            else:
                                file = file + '_prediction_NONE'
                                iteration = 100
                                ant = 10
                            file = file + '_iterations_' + str(iteration)
                            file = file + '_ants_' + str(ant)
                            file = file + '_round_'
                            for ro in rounds:
                                filer = file + str(ro) + ".csv"
                                with open(filer, newline='') as csvfile:
                                    csvData = csv.reader(csvfile, delimiter=',', quotechar='|')
                                    r = 0
                                    for row in csvData:
                                        if r == 0:
                                            headers = ['time', 'approach', 'round', 'type', 'latency','utilisation','waiting']
                                        if r > 0:
                                            a = row[0].split(':')
                                            time = int(a[0].replace('"',''))
                                            d = []
                                            d = [time, approach.upper(), str(ro), row[1].replace('"', ''), float(row[2].replace('"', '')), float(row[3].replace('"', '')), (float(row[4].replace('"', ''))/1000)]
                                            data.append(d)
                                        r = r + 1
                
                for baseline in baselines:
                    file = './data/'+baseline+'/decentralised/'
                    file = file + 'approach_'+baseline
                    file = file + '_scenario_' + str(scenario)
                    file = file + '_edge_servers_' + str(server)
                    file = file + '_mec_systems_5'
                    file = file + '_functions_' + str(function)
                    file = file + '_prediction_NONE'
                    file = file + '_round_'
                    for ro in rounds:
                        filer = file + str(ro) + ".csv"
                        with open(filer, newline='') as csvfile:
                            csvData = csv.reader(csvfile, delimiter=',', quotechar='|')
                            r = 0
                            for row in csvData:
                                if r == 0:
                                    headers = ['time', 'approach', 'round', 'type', 'latency','utilisation','waiting']
                                if r > 0:
                                    a = row[0].split(':')
                                    time = int(a[0].replace('"',''))
                                    d = []
                                    d = [time, baseline.upper(), str(ro), row[1].replace('"', ''), float(row[2].replace('"', '')), float(row[3].replace('"', '')), (float(row[4].replace('"', ''))/1000)]
                                    data.append(d)
                                r = r + 1
                            
                results = pd.DataFrame(data, columns=headers)    
                results = results.groupby(['approach', 'type', 'round']).mean()
                results.reset_index(level=2, inplace=True)
                results.reset_index(level=1, inplace=True)
                results.reset_index(level=0, inplace=True)
                #filtered = results[results.time >= 20]
                #filtered = filtered[results.time < 22]
                
                #fig = plt.figure(figsize=(15, 15))  
                #fig.suptitle('Network Latency', fontsize=LARGE_SIZE)      
                sns.barplot(x='approach', y='latency', hue='type', data=results, ax=axs1[i, j])
                #sns.violinplot(x='approach', y='latency', hue='type', data=results, ax=axs1[i, j], showfliers=True)
                #sns.boxplot(x='approach', y='latency', hue='type', data=results, ax=axs1[i, j], showfliers=False)
                if scenario == 1:
                    if function == 1:
                        axs1[i, j].set_ylim(20, 60)
                    if function == 3:
                        axs1[i, j].set_ylim(20, 125)
                    if function == 5:
                        axs1[i, j].set_ylim(20, 190)
                if function == 1:
                    axs1[i, j].set(title=(str(function) + ' Service and ' + str(server) + ' Servers'))
                else:
                    axs1[i, j].set(title=(str(function) + ' Services and ' + str(server) + ' Servers'))
                axs1[i, j].set(xlabel=('Approach'))
                axs1[i, j].set(ylabel=('Milliseconds (ms)'))
                axs1[i, j].grid(linestyle='-', linewidth='1.0', color='grey')
                axs1[i, j].set_xticklabels(axs1[i, j].get_xticklabels(), rotation=90)
                handles, labels = axs1[i, j].get_legend_handles_labels()
                axs1[i, j].legend(handles, labels, loc = 'lower right', ncol=3)
                fig1.tight_layout(rect=[0, 0.03, 1, 0.95])                
                
                #fig = plt.figure(figsize=(15, 15))  
                #fig.suptitle('Resource Utilisation', fontsize=LARGE_SIZE)      
                sns.barplot(x='approach', y='utilisation', hue='type', data=results, ax=axs2[i, j])
                #sns.violinplot(x='approach', y='utilisation', hue='type', data=results, ax=axs2[i, j], showfliers=True)
                #sns.boxplot(x='approach', y='utilisation', hue="type", data=results, ax=axs2[i, j], showfliers=False)
                if function == 1:
                    axs2[i, j].set(title=(str(function) + ' Service and ' + str(server) + ' Servers'))
                else:
                    axs2[i, j].set(title=(str(function) + ' Services and ' + str(server) + ' Servers'))
                axs2[i, j].set(xlabel=('Approach'))
                axs2[i, j].set(ylabel=('Standard Deviation (\u03C3)'))
                axs2[i, j].grid(linestyle='-', linewidth='1.0', color='grey')
                axs2[i, j].set_xticklabels(axs2[i, j].get_xticklabels(), rotation=90)
                handles, labels = axs2[i, j].get_legend_handles_labels()
                if scenario == 1:
                    if (function == 1 and server > 100) or (function == 3 and server > 200):
                        axs2[i, j].legend(handles, labels, loc = 'upper right', ncol=3)
                    elif (function == 5):
                        axs2[i, j].legend(handles, labels, loc = 'upper right', ncol=1)
                    else:
                        axs2[i, j].legend(handles, labels, loc = 'lower right', ncol=3)                                    
                        
                    if function == 1:
                        axs2[i, j].set_ylim(0.05, 0.35)
                    if function == 3:
                        axs2[i, j].set_ylim(0.1, 0.45)
                    if function == 5:
                        axs2[i, j].set_ylim(0.1, 0.8)
                if scenario == 2 or scenario == 3:
                    axs2[i, j].legend(handles, labels, loc = 'lower right', ncol=3)
                fig2.tight_layout(rect=[0, 0.03, 1, 0.95])
                
                #fig = plt.figure(figsize=(15, 15))  
                #fig.suptitle('Waiting Time', fontsize=LARGE_SIZE)      
                sns.barplot(x='approach', y='waiting', hue='type', data=results, ax=axs3[i, j])
                #sns.violinplot(x='approach', y='waiting', hue='type', data=results, ax=axs3[i, j], showfliers=True)
                #sns.boxplot(x='approach', y='waiting', hue='type', data=results, ax=axs3[i, j], showfliers=False)
                if function == 1:
                    axs3[i, j].set(title=(str(function) + ' Service and ' + str(server) + ' Servers'))
                else:
                    axs3[i, j].set(title=(str(function) + ' Services and ' + str(server) + ' Servers'))
                axs3[i, j].set(xlabel=('Approach'))
                axs3[i, j].set(ylabel=('Seconds (s)'))
                axs3[i, j].grid(linestyle='-', linewidth='1.0', color='grey')
                axs3[i, j].set_xticklabels(axs3[i, j].get_xticklabels(), rotation=90)
                handles, labels = axs3[i, j].get_legend_handles_labels()
                if scenario == 1:
                    if (function > 1 and server == 100) or (function == 5 and server == 200):
                        axs3[i, j].legend(handles, labels, loc = 'upper right', ncol=1)
                    else:
                        axs3[i, j].legend(handles, labels, loc = 'lower right', ncol=3)
                if scenario == 2 or scenario == 3:
                    axs3[i, j].legend(handles, labels, loc = 'lower right', ncol=3)
                fig3.tight_layout(rect=[0, 0.03, 1, 0.95])
                
                data = []
                j = j + 1
            i = i + 1
        fig1.savefig('figs/all-latency-scenario' + str(scenario) + '.pdf')            
        fig2.savefig('figs/all-utilisation-scenario' + str(scenario) + '.pdf')            
        fig3.savefig('figs/all-waiting-scenario' + str(scenario) + '.pdf')

# This function plots the heatmaps that include maaco approaches results
def plot_maaco_rounds():
    SMALL_SIZE = 28
    MEDIUM_SIZE = 32
    LARGE_SIZE = 32
    plt.rc('font', size=SMALL_SIZE, family='Times New Roman')          
    plt.rc('axes', titlesize=MEDIUM_SIZE)
    plt.rc('axes', labelsize=MEDIUM_SIZE)    
    plt.rc('axes', linewidth=2.0)
    plt.rc('xtick', labelsize=24)    
    plt.rc('ytick', labelsize=24)    
    plt.rc('legend', fontsize=SMALL_SIZE)
    plt.rc('figure', titlesize=LARGE_SIZE)
    
    data = []
    headers = []
    approaches = []
    approaches = ['maaco-BC', 'maaco-HMM']
    scenarios = [1]
    #scenarios = [1, 2, 3]
    #servers = [100, 300]
    servers = [100, 200, 300] 
    #functions = [1]
    functions = [1, 3, 5]
    iterations = [10, 100, 1000]
    #iterations = [10, 100]
    ants = [10, 100, 1000]
    #ants = [10, 100, 1000]
    rounds = [1, 2, 3, 4, 5]
    
    for scenario in scenarios:
        fig1, axs1 = plt.subplots(3, 3, figsize=(20, 30))
        fig2, axs2 = plt.subplots(3, 3, figsize=(20, 30))
        fig3, axs3 = plt.subplots(3, 3, figsize=(20, 30))
        """if scenario == 1:
            fig1.suptitle('Average Network Latency (ms) Scenario 1: $\u03bc_s > \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig2.suptitle('Resource Utilisation (\u03C3) Scenario 1: $\u03bc_s > \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig3.suptitle('Average Waiting Time (s) Scenario 1: $\u03bc_s > \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
        if scenario == 2:
            fig1.suptitle('Average Network Latency (ms) Scenario 2: $\u03bc_s < \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig2.suptitle('Resource Utilisation (\u03C3) Scenario 2: $\u03bc_s < \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig3.suptitle('Average Waiting Time (s) Scenario 2: $\u03bc_s < \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
        if scenario == 3:
            fig1.suptitle('Average Network Latency (ms) Scenario 3: $\u03bc_s = \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig2.suptitle('Resource Utilisation (\u03C3) Scenario 3: $\u03bc_s = \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)
            fig3.suptitle('Average Waiting Time (s) Scenario 3: $\u03bc_s = \u03BB_A, \u03BB_O, \u03BB_G$', fontsize=LARGE_SIZE)"""
        i = 0
        for function in functions:
            j = 0
            for server in servers:
                for approach in approaches:
                    for iteration in iterations:
                        for ant in ants:
                            if approach == 'maaco-HMM':
                                file = './data/maaco/decentralised/'
                                file = file + 'approach_maaco'
                            elif approach == 'maaco-BC':
                                file = './data/maaco/decentralised/'
                                file = file + 'approach_maaco'
                            
                            file = file + '_scenario_' + str(scenario)
                            file = file + '_edge_servers_' + str(server)
                            file = file + '_mec_systems_5'
                            file = file + '_functions_' + str(function)
                            if approach == 'maaco-HMM':
                                file = file + '_prediction_HMM'
                            elif approach == 'maaco-BC':
                                file = file + '_prediction_BC'
                            
                            file = file + '_iterations_' + str(iteration)
                            file = file + '_ants_' + str(ant)
                            file = file + '_round_'
                            for ro in rounds:
                                filer = file + str(ro) + ".csv"
                                try:
                                    with open(filer, newline='') as csvfile:
                                        csvData = csv.reader(csvfile, delimiter=',', quotechar='|')
                                        r = 0
                                        for row in csvData:
                                            if r == 0:
                                                headers = ['time', 'approach', 'round', 'type', 'latency','utilisation','waiting']
                                            if r > 0:
                                                a = row[0].split(':')
                                                time = int(a[0].replace('"',''))
                                                d = []
                                                app = ''
                                                d = [time, approach.upper().replace('MAACO-', '')+ '-' +  str(iteration) + ' I -' + str(ant) + ' A' , str(ro), row[1].replace('"', ''), float(row[2].replace('"', '')), float(row[3].replace('"', '')), (float(row[4].replace('"', ''))/1000)]
                                                data.append(d)
                                            r = r + 1
                                except:
                                    print("File not found: " + filer)
                
                results = pd.DataFrame(data, columns=headers)    
                results = results.groupby(['approach', 'type', 'round']).mean()
                results.reset_index(level=2, inplace=True)
                results.reset_index(level=1, inplace=True)
                results.reset_index(level=0, inplace=True)
                #filtered = results[results.time >= 20]
                #filtered = filtered[results.time < 22]
                
                fp = results.pivot_table(index='approach', columns='type', values='latency')
                sns.heatmap(fp, annot=True, fmt='.1f', cmap='YlGnBu', linecolor='black', linewidths=1, ax=axs1[i, j])
                if function == 1:
                    axs1[i, j].set(title=(str(function) + ' Service and ' + str(server) + ' Servers'))
                else:
                    axs1[i, j].set(title=(str(function) + ' Services and ' + str(server) + ' Servers'))
                if function == 5:
                    axs1[i, j].set(xlabel=('Type'))
                    axs1[i, j].set_xticklabels(axs1[i, j].get_xticklabels(), rotation=30)
                else:
                    axs1[i, j].set(xlabel=(''))
                    axs1[i, j].set_xticklabels([])
                if server == 100:
                    axs1[i, j].set(ylabel=('Approach'))
                    axs1[i, j].set_yticklabels(axs1[i, j].get_yticklabels(), rotation=360)
                else:
                    axs1[i, j].set(ylabel=(''))
                    axs1[i, j].set_yticklabels([])
                fig1.tight_layout(rect=[0, 0.03, 1, 0.95])                
                
                fp = results.pivot_table(index='approach', columns='type', values='utilisation')
                sns.heatmap(fp, annot=True, fmt='.1f', cmap='YlGnBu', linecolor='black', linewidths=1, ax=axs2[i, j])
                if function == 1:
                    axs2[i, j].set(title=(str(function) + ' Service and ' + str(server) + ' Servers'))
                else:
                    axs2[i, j].set(title=(str(function) + ' Services and ' + str(server) + ' Servers'))
                if function == 5:
                    axs2[i, j].set(xlabel=('Type'))
                    axs2[i, j].set_xticklabels(axs1[i, j].get_xticklabels(), rotation=30)
                else:
                    axs2[i, j].set(xlabel=(''))
                    axs2[i, j].set_xticklabels([])
                if server == 100:
                    axs2[i, j].set(ylabel=('Approach'))
                    axs2[i, j].set_yticklabels(axs1[i, j].get_yticklabels(), rotation=360)
                else:
                    axs2[i, j].set(ylabel=(''))
                    axs2[i, j].set_yticklabels([])
                fig2.tight_layout(rect=[0, 0.03, 1, 0.95])
                
                fp = results.pivot_table(index='approach', columns='type', values='waiting')
                sns.heatmap(fp, annot=True, fmt='.1f', cmap='YlGnBu', linecolor='black', linewidths=1, ax=axs3[i, j])
                if function == 1:
                    axs3[i, j].set(title=(str(function) + ' Service and ' + str(server) + ' Servers'))
                else:
                    axs3[i, j].set(title=(str(function) + ' Services and ' + str(server) + ' Servers'))
                if function == 5:
                    axs3[i, j].set(xlabel=('Type'))
                    axs3[i, j].set_xticklabels(axs1[i, j].get_xticklabels(), rotation=30)
                else:
                    axs3[i, j].set(xlabel=(''))
                    axs3[i, j].set_xticklabels([])
                if server == 100:
                    axs3[i, j].set(ylabel=('Approach'))
                    axs3[i, j].set_yticklabels(axs1[i, j].get_yticklabels(), rotation=360)
                else:
                    axs3[i, j].set(ylabel=(''))
                    axs3[i, j].set_yticklabels([])
                    
                fig3.tight_layout(rect=[0, 0.03, 1, 0.95])
                
                """#fig = plt.figure(figsize=(15, 15))  
                #fig.suptitle('Network Latency', fontsize=LARGE_SIZE)      
                sns.barplot(x='approach', y='latency', hue='type', data=results, ax=axs1[i, j])
                #sns.violinplot(x='approach', y='latency', hue='type', data=results, ax=axs1[i, j], showfliers=True)
                #sns.boxplot(x='approach', y='latency', hue='type', data=results, ax=axs1[i, j], showfliers=False)
                if scenario == 1:
                    if function == 1:
                        axs1[i, j].set_ylim(20, 60)
                    if function == 3:
                        axs1[i, j].set_ylim(45, 125)
                    if function == 5:
                        axs1[i, j].set_ylim(20, 190)
                axs1[i, j].set(title=(str(function) + ' Functions and ' + str(server) + ' Servers'))
                axs1[i, j].set(xlabel=('Approach'))
                axs1[i, j].set(ylabel=('Milliseconds (ms)'))
                axs1[i, j].grid(linestyle='-', linewidth='1.0', color='grey')
                axs1[i, j].set_xticklabels(axs1[i, j].get_xticklabels(), rotation=90)
                handles, labels = axs1[i, j].get_legend_handles_labels()
                axs1[i, j].legend(handles, labels, loc = 'lower right', ncol=3)
                if scenario == 1:
                    if function >= 3:
                        axs1[i, j].legend(handles, labels, loc = 'upper right', ncol=3)
                fig1.tight_layout(rect=[0, 0.03, 1, 0.95])                
                
                #fig = plt.figure(figsize=(15, 15))  
                #fig.suptitle('Resource Utilisation', fontsize=LARGE_SIZE)      
                sns.barplot(x='approach', y='utilisation', hue='type', data=results, ax=axs2[i, j])
                #sns.violinplot(x='approach', y='utilisation', hue='type', data=results, ax=axs2[i, j], showfliers=True)
                #sns.boxplot(x='approach', y='utilisation', hue="type", data=results, ax=axs2[i, j], showfliers=False)
                axs2[i, j].set(title=(str(function) + ' Functions and ' + str(server) + ' Servers'))
                axs2[i, j].set(xlabel=('Approach'))
                axs2[i, j].set(ylabel=('Standard Deviation (\u03C3)'))
                axs2[i, j].grid(linestyle='-', linewidth='1.0', color='grey')
                axs2[i, j].set_xticklabels(axs2[i, j].get_xticklabels(), rotation=90)
                handles, labels = axs2[i, j].get_legend_handles_labels()
                if scenario == 1:
                    axs2[i, j].legend(handles, labels, loc = 'upper right', ncol=3)
                    if function == 1:
                        axs2[i, j].set_ylim(0.05, 0.35)
                    if function == 3:
                        axs2[i, j].set_ylim(0.1, 0.55)
                    if function == 5:
                        axs2[i, j].set_ylim(0.1, 1.10)
                fig2.tight_layout(rect=[0, 0.03, 1, 0.95])
                
                #fig = plt.figure(figsize=(15, 15))  
                #fig.suptitle('Waiting Time', fontsize=LARGE_SIZE)      
                sns.barplot(x='approach', y='waiting', hue='type', data=results, ax=axs3[i, j])
                #sns.violinplot(x='approach', y='waiting', hue='type', data=results, ax=axs3[i, j], showfliers=True)
                #sns.boxplot(x='approach', y='waiting', hue='type', data=results, ax=axs3[i, j], showfliers=False)
                axs3[i, j].set(title=(str(function) + ' Functions and ' + str(server) + ' Servers'))
                axs3[i, j].set(xlabel=('Approach'))
                axs3[i, j].set(ylabel=('Seconds (s)'))
                axs3[i, j].grid(linestyle='-', linewidth='1.0', color='grey')
                axs3[i, j].set_xticklabels(axs3[i, j].get_xticklabels(), rotation=90)
                handles, labels = axs3[i, j].get_legend_handles_labels()
                if scenario == 1:
                    axs3[i, j].legend(handles, labels, loc = 'upper right', ncol=3)
                fig3.tight_layout(rect=[0, 0.03, 1, 0.95])"""
                
                data = []
                j = j + 1
            i = i + 1
        fig1.savefig('figs/maaco-latency-scenario' + str(scenario) + '.pdf')            
        fig2.savefig('figs/maaco-utilisation-scenario' + str(scenario) + '.pdf')            
        fig3.savefig('figs/maaco-waiting-scenario' + str(scenario) + '.pdf')

def main():
    
    # Plot all evaluated approaches
    # plot_all_rounds()
    
    # Plot maaco approaches
    plot_maaco_rounds()
    
    
main()